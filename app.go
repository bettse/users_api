package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	zipkin "github.com/openzipkin/zipkin-go"
	zipkinhttp "github.com/openzipkin/zipkin-go/middleware/http"
	httpreporter "github.com/openzipkin/zipkin-go/reporter/http"
	log "github.com/sirupsen/logrus"
)

type App struct {
	Router *mux.Router
}

type User struct {
	ID int `json:"ID"`
	// https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/
	First string `json:"First"`
	Last  string `json:"Last"`
	Zip   string `json:"Zip"` // Store as string to preserve leading zero
	Email string `json:"Email"`
}

var users = make(map[int]User)
var nextIndex = 1

func (a *App) Initialize() {
	log.SetFormatter(&log.JSONFormatter{})
	a.Router = mux.NewRouter().StrictSlash(true)
	a.initializeRoutes()
}

func (a *App) Run(addr string) {
	serverMiddleware := a.initializeOpenTracing()
	log.WithFields(log.Fields{"addr": addr}).Info("Server Started")
	// https://medium.com/@nate510/don-t-use-go-s-default-http-client-4804cb19f779
	// Swap http.ListenAndServe with an instanciated copy before using in production
	log.Fatal(http.ListenAndServe(addr, serverMiddleware(a.Router)))
}

// Zipkin setup pulled blantently from https://github.com/openzipkin/zipkin-go/blob/master/example_httpserver_test.go
func (a *App) initializeOpenTracing() func(http.Handler) http.Handler {
	// set up a span reporter
	reporter := httpreporter.NewReporter("http://localhost:9411/api/v2/spans")
	defer reporter.Close()

	// create our local service endpoint
	endpoint, err := zipkin.NewEndpoint("Users API", "localhost:0")
	if err != nil {
		log.Fatalf("unable to create local endpoint: %+v\n", err)
	}

	// initialize our tracer
	tracer, err := zipkin.NewTracer(reporter, zipkin.WithLocalEndpoint(endpoint))
	if err != nil {
		log.Fatalf("unable to create tracer: %+v\n", err)
	}

	// create global zipkin http server middleware
	return zipkinhttp.NewServerMiddleware(
		tracer, zipkinhttp.TagResponseSize(true),
	)
}

func (a *App) initializeRoutes() {
	// Normally we'd have middleware here for auth/validation/logging/tracing, etc
	a.Router.HandleFunc("/users", a.createUser).Methods("POST")
	a.Router.HandleFunc("/users/{id}", a.readUser).Methods("GET")
	// For simplicity I went with "PUT", which accepts a complete new record, instead of "PATH", which accepts only the fields to update
	a.Router.HandleFunc("/users/{id}", a.updateUser).Methods("PUT")
	a.Router.HandleFunc("/users/{id}", a.deleteUser).Methods("DELETE")
	a.Router.HandleFunc("/users", a.listUsers).Methods("GET")
}

func (a *App) createUser(w http.ResponseWriter, r *http.Request) {
	var newUser User
	if r.Body == nil {
		respondWithError(w, http.StatusBadRequest, "Missing POST body")
		return
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Unable to read body of request")
		return
	}

	// Normally we'd want to do some validation based on system requirements or input from Product to determine what a minimal user record is.
	json.Unmarshal(reqBody, &newUser)

	newUser.ID = nextIndex
	nextIndex += 1
	users[newUser.ID] = newUser
	log.WithFields(log.Fields{"userId": newUser.ID}).Info("createUser")

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newUser)
}

func (a *App) readUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Error parsing user ID")
		return
	}
	log.WithFields(log.Fields{"userId": userID}).Info("readUser")

	user, ok := users[userID]
	if !ok {
		respondWithError(w, http.StatusNotFound, "No such user ID")
		return
	}
	json.NewEncoder(w).Encode(user)
}

func (a *App) listUsers(w http.ResponseWriter, r *http.Request) {
	log.Info("listUsers")
	userArray := []User{}
	for _, user := range users {
		userArray = append(userArray, user)
	}
	json.NewEncoder(w).Encode(userArray)
}

func (a *App) updateUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Error parsing user ID")
		return
	}
	var userUpdate User
	log.WithFields(log.Fields{"userId": userID}).Info("updateUser")

	user, ok := users[userID]
	if !ok {
		respondWithError(w, http.StatusNotFound, "No such user ID")
		return
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Unable to read body of request")
		return
	}
	json.Unmarshal(reqBody, &userUpdate)

	user.First = userUpdate.First
	user.Last = userUpdate.Last
	user.Zip = userUpdate.Zip
	user.Email = userUpdate.Email

	users[userID] = user

	json.NewEncoder(w).Encode(user)
}

func (a *App) deleteUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Error parsing user ID")
		return
	}
	log.WithFields(log.Fields{"userId": userID}).Info("updateUser")

	_, ok := users[userID]
	if !ok {
		respondWithError(w, http.StatusNotFound, "No such user ID")
		return
	}

	delete(users, userID)
	respondWithJSON(w, http.StatusNoContent, nil)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
