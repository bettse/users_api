module users_api

require (
	github.com/gorilla/mux v1.7.3
	github.com/opentracing-contrib/go-gorilla v0.0.0-20190110000444-ced666783644
	github.com/opentracing-contrib/go-stdlib v0.0.0-20190519235532-cf7a6c988dc9
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/openzipkin/zipkin-go v0.2.2
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
)

go 1.13
