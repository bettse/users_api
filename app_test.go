package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

type vars map[string]string

var a App

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	a = App{}
	a.Initialize()
}

func TestCreateUser(t *testing.T) {
	user := createExampleUser()
	assert.NotEmpty(t, user.ID)
}

func TestCreateUserWithNoBody(t *testing.T) {
	req, _ := http.NewRequest("POST", "/users", nil)
	response := executeRequest(req)
	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestCreateUserWithEmptyBody(t *testing.T) {
	body := []byte{}
	req, _ := http.NewRequest("POST", "/users", bytes.NewBuffer(body))
	response := executeRequest(req)
	assert.Equal(t, http.StatusCreated, response.Code)
}

func TestReadUser(t *testing.T) {
	exampleUser := createExampleUser()
	userID := strconv.Itoa(exampleUser.ID)

	req, _ := http.NewRequest("GET", "/users/"+userID, nil)
	response := executeRequest(req)
	expected, _ := json.Marshal(exampleUser)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, string(expected), response.Body.String())
}

func TestReadUserWithStringId(t *testing.T) {
	req, _ := http.NewRequest("GET", "/users/x", nil)
	response := executeRequest(req)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestUpdateUser(t *testing.T) {
	exampleUser := createExampleUser()
	userID := strconv.Itoa(exampleUser.ID)

	exampleUser.Email = "bettse@gmail.com"
	body, _ := json.Marshal(exampleUser)

	req, _ := http.NewRequest("PUT", "/users/"+userID, bytes.NewBuffer(body))
	response := executeRequest(req)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, string(body), response.Body.String())
}

func TestUpdateUserThatDoesntExist(t *testing.T) {
	req, _ := http.NewRequest("PUT", "/users/10", nil)
	response := executeRequest(req)

	assert.Equal(t, http.StatusNotFound, response.Code)
}

func TestUpdateUserWithStringId(t *testing.T) {
	req, _ := http.NewRequest("PUT", "/users/x", nil)
	response := executeRequest(req)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestDeleteUsers(t *testing.T) {
	exampleUser := createExampleUser()
	userID := strconv.Itoa(exampleUser.ID)

	req, _ := http.NewRequest("DELETE", "/users/"+userID, nil)
	response := executeRequest(req)

	assert.Equal(t, http.StatusNoContent, response.Code)

	readReq, _ := http.NewRequest("GET", "/users/"+userID, nil)
	readResponse := executeRequest(readReq)
	assert.Equal(t, http.StatusNotFound, readResponse.Code)
}

func TestDeleteUserWithStringId(t *testing.T) {
	req, _ := http.NewRequest("DELETE", "/users/x", nil)
	response := executeRequest(req)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func TestDeleteUserThatDoesntExist(t *testing.T) {
	req, _ := http.NewRequest("DELETE", "/users/10", nil)
	response := executeRequest(req)

	assert.Equal(t, http.StatusNotFound, response.Code)
}

func TestListUsers(t *testing.T) {
	createExampleUser()
	createExampleUser()
	createExampleUser()
	userList := []User{}

	req, _ := http.NewRequest("GET", "/users", nil)
	response := executeRequest(req)
	json.Unmarshal(response.Body.Bytes(), &userList)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.NotEmpty(t, userList)
}

func TestInitializeOpenTracing(t *testing.T) {
	serverMiddleware := a.initializeOpenTracing()
	assert.NotNil(t, serverMiddleware)
}

func createExampleUser() User {
	exampleUser := User{
		First: "Eric",
		Last:  "Betts",
		Zip:   "97205",
		Email: "bettse@fastmail.fm",
	}

	body, _ := json.Marshal(exampleUser)
	req, _ := http.NewRequest("POST", "/users", bytes.NewBuffer(body))
	response := executeRequest(req)

	var newUser User
	json.Unmarshal(response.Body.Bytes(), &newUser)
	return newUser
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	return w
}
