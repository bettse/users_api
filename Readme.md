# Users API

## Spec

Create a micro-service with the following:

- A User rest Resource that allows clients to create, read, update, delete a user or a list of users.
- You can use a database of your choice but it's also fine to just use a map or dictionary in memory to keep track of users by their ids.
- Use structured logging
- Add Metrics (such as dropwizard, codahale, or prometheus to time routes)
- Write unit tests for the service.
- Generate a code coverage report (for Java you can use the jacoco-maven-plugin for unit test coverage).
- The user JSON can just be id, first name, last name, zip code, and email address.
- You can use Java, GoLang, or Python for this exercise.
  - If Java, the User class should be immutable.

### Feedback

> The folks reviewing your submission highlighted the comments and error handling in your implementation as strong points, though overall felt that it did not show the level of mastery that we are currently searching for, and as a result we won't be moving forward with your candidacy for this particular role.

## Run

- For non-production purposes, execute using `go run`: `go run main.go app.go`
  - TODO: Add [realize](https://github.com/oxequa/realize) to auto-reload server on file change during development

## Metrics / instrumentation

### OpenTracing using Zipkin

For metrics I've added OpenTracing, using the Zipkin instrumentation since its can be run as a docker container

- Start Zipkin: `docker run -d -p 9411:9411 openzipkin/zipkin`
- Start the service and exercise it
- Open your browser: [http://localhost:9411](http://localhost:9411)
- Select "users api" from "Service Name" dropdown and click "Find Traces"
- Selecting a trace opens a new window with its spans, but for this simple service, there is only the single parent span.

NB: If you see log messages

> failed to send the request: Post http://localhost:9411/api/v2/spans: dial tcp [::1]:9411: connect: connection refused

this means it can't report the traces back to the zipkin container. If you're not interested in metrics, feel free to ignore.

## Test

### Run

- Execute tests as usual for golang: `go test`

### Coverage

- Using this oneliner will calculate test coverage and open your browser with the color coded source: `go test -coverprofile=c.out && go tool cover -html=c.out`
- Using this oneliner will calculate test coverage and produce a coverage.html with color coded source: `go test -coverprofile=c.out && go tool cover -html=c.out -o coverage.html`

## Inspiration

Heavily based on the following:

- https://medium.com/the-andela-way/build-a-restful-json-api-with-golang-85a83420c9da
- https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql
